---
layout: show
name: "This Ends Now"
year: 2011
poster: "this_ends_now.jpg"
---

McCormick Auditorium
Northwestern University

### Pieces/Writers

#### ACT I

Opener - Aaron Faucher and Lori Buchanan

Stool Sample - Justin Lueker

(Zombeats) - Jed Feder

Old Machines - Josh Brechner and Aaron Faucher

Sail - Alyssa Beebe

Skillet - Ravi Chopra, Josh Brechner, Paul Marino

(Jalen’s transition)

B17 - Andrew Abramowitz and Alyssa Beebe

Duet - Ian Holden, Suzanne Hutt, Austin Walker, Aaron Faucher

TimeFuck - Paul Marino

(Jerk vs. Dougie) - Aaron Faucher and Chika Nwosu

Afterparty - Ravi Chopra and Suzanne Hutt

#### ACT II
 
Smorgasm - Josh Brechner and Margaret Tudor

Tomahawk - Jed Feder

(Senior Transition) - Jed Feder, Austin Walker, Ravi Chopra, Chika Nwosu, Lori Buchanan

The OC Season 2 - Chika Nwosu and Oluchi Nwosu

It’s About Time - Austin Walker

(Biggie Friends) - Justin Lueker

146 True - Lori Buchanan

Tadpoles - Jed Feder

Closer - Austin Walker and Suzanne Hutt

### Featured Music

1. Spitfire – The Prodigy (Featured in Opener)
2. Switch Up – Pretty Lights
3. Too Many Feelings – Javelin
4. Somesing – Sound Tribe Sector 9
5. Heart Skipped a Beat – The xx (Featured in Sexual Tension)
6. Contingent Being – SaReGaMa
7. Imitosis (Remix) – Andre Bird/Four Tet
8. Chewing Gum (Vocal Mix) – Annie
9. Let Me – Rhianna; (Featured in the O.C.)
10. Getting Freaky – Play N Skillz ft. Pitbull (Featured in the O.C.)
11. Arcadia – Apparat
12. Pattern – Zapan
13. Seattle – This is Art (Featured in Red Vs. Blue)
14. Deeper – Kid Static
15. A Vida - Cujo
16. Dee – Javelin
17. Sacred Place – Future of Forestry (Featured in Awkward Suicide)
18. Cosmic Cycles – This is Art
19. Selection from the Osmos soundtrack – composed by Vincent et Tristan, Julien Neto, Gas 0095, Loscil, High Skies (Featured in Zen      Tower)
20. Life and Times Intro – Josh Brechner
21. Upbeet – Bookmobile
22. A New Conviction – Ve
23. Letter from God to Man – Dan Le Sac vs. Scroobius Pip (Featured       in Closer)
24. Susie Cues - Javelin