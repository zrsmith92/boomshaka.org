---
layout: show
name: "Restore Chaos"
year: 2008
poster: "restore_chaos.jpg"
---

McCormick Auditorium
Northwestern University

5/15 at 8 pm, 5/16 at 8 & 11p.m., and 5/17 at 8 & 11p.m.

### Pieces/Writers

#### ACT I 

Opener - Jeff Krauss, Genevieve Garcia, Harrison Ude, Alex Hersler, Nicole West, Tom Roth, Gabe Bitto

Da Heebs - Annie Kahane, Alex Hersler

What First - Annie Kahane

(Silly Hats - Gabe Bitto)

The Food - Harrison Ude

The Machine - Laura Merchut

(J's iPod - Jeff Krauss)

Sticky - Dashiell Oatman-Stanford

(Step It - Emily Dolan)

2.lovers - Harrison Ude, Genevieve Garcia

Stone Ground 8 - Jin Lee

Evaporate - Gabe Bitto, Genevieve Garcia

#### ACT II 

It's Not A Rice Cooker, It's My Boom Box - Jessica Saltiel

(Connect 4 - Alex Hersler, Lori Buchanan)

No Man Is An Island - Alex Hersler

(Snaps Transition - Reed Walker)

Nerve - Blake McKay, Chika Nwosu

18.9 - Tom Roth

ROCKSTAR!! - Nicole West, Jason Choi

(Drywall - Tom Roth, Alex Hersler)

8!BIT - Jeff Krauss

Reminisce - Austin Walker

(Senior Transition - Gabe Bitto, Alex Hersler, Laura Merchut, Tom Roth)

Poles - Gabe Bitto, Alex Hersler

Closer - Jeff Krauss, Genevieve Garcia, Harrison Ude, Gabe Bitto, Tom Roth, Laura Merchut, Alex Hersler, Annie Kahane, Nicole West