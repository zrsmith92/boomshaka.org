---
layout: show
name: "Stomping Ground"
year: 2010
poster: "stomping_ground.jpg"
---

McCormick Auditorium
Northwestern University

### Pieces/Writers

#### ACT I

Opener - Annie Kahane and Jed Feder

Find Your Porpoise – Jed Feder and Justin Lueker

6Stroke – Austin Walker

DJ – Emily Dolan and Justil Lueker

BoomBox – Ian Holden

Sexual Tension – Suzanne Hutt

Basic – Jed Feder

(Mario Brothers)

The O.C. – Chika Nwosu and Oluchi Nwosu

Straight Shots – Ravi Chopra and Alyssa Beebe

Trio – Annie Kahane

(Mini Red vs. Blue)

Red vs. Blue – Paul Marino and Josh Brechner

#### ACT II
 
KrunkB – Lori Buchanan and Ravi Chopra

Big Kids – Austin Walker and Chika Nwosu

(Runaway Biggie)

Awkward Suicide – Margaret Tudor

Guys Drumming – Ravi Chopra and Josh Brechner

(Zen Tower)

Life & Times – Josh Brechner and Alyssa Beebe

Poles – Austin Walker and Lori Buchanan

Closer – Austin Walker and Lori Buchanan

### Featured Music

1. Spitfire – The Prodigy (Featured in Opener)
2. Switch Up – Pretty Lights
3. Too Many Feelings – Javelin
4. Somesing – Sound Tribe Sector 9
5. Heart Skipped a Beat – The xx (Featured in Sexual Tension)
6. Contingent Being – SaReGaMa
7. Imitosis (Remix) – Andre Bird/Four Tet
8. Chewing Gum (Vocal Mix) – Annie
9. Let Me – Rhianna; (Featured in the O.C.)
10. Getting Freaky – Play N Skillz ft. Pitbull (Featured in the O.C.)
11. Arcadia – Apparat
12. Pattern – Zapan
13. Seattle – This is Art (Featured in Red Vs. Blue)
14. Deeper – Kid Static
15. A Vida - Cujo
16. Dee – Javelin
17. Sacred Place – Future of Forestry (Featured in Awkward Suicide)
18. Cosmic Cycles – This is Art
19. Selection from the Osmos soundtrack – composed by Vincent et Tristan, Julien Neto, Gas 0095, Loscil, High Skies (Featured in Zen      Tower)
20. Life and Times Intro – Josh Brechner
21. Upbeet – Bookmobile
22. A New Conviction – Ve
23. Letter from God to Man – Dan Le Sac vs. Scroobius Pip (Featured       in Closer)
24. Susie Cues - Javelin