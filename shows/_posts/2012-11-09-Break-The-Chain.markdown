---
layout: show
name: "Break The Chain"
year: 2012
poster: "break_the_chain.jpg"
---

McCormick Auditorium
Northwestern University

### Pieces/Writers

#### ACT I

Opener - Josh Brechner, Alyssa Beebe, Jourdin Batchelor, Zach Smith, Zach Hyman

Meat - Justin Lueker

Swoops - Suzanne Hutt

Footwork Transition - Zach Smith and maybe Justin Lueker (?)

Moustache Cash Stash - Jalen Motes

Hip Hop - Oluchi Nwosu and Uche Nwosu

Spoken Word (Transition) - Paul Marino

Slugz on Drugz - Josh Brechner

Flubber - Aaron Faucher and Melanie Nekhorn

Senior Transition - Alyssa Beebe, Josh Brechner, Aaron Faucher, Ian Holden

Powderpuff - Zach Smith and Jourdin Batchelor

No Light - Margaret Tudor

Clam Jam - Justin Lueker and Suzanne Hutt

#### ACT II
 
Echoes - Aaron Faucher

Suspension - Zach Hyman and Madeline Hooper

BananaFishSquid (Transition) - Josh Brechner

Triptych - Ian Holden

Cooking With Boomshaka (Transition) - Zach Hyman

Concrete Walls - Alyssa Beebe

Ellipsis - Aaron Faucher

A Poleish Bicycle - Justin Lueker and Andrew Abramowitz

Closer - Margaret Tudor, Paul Marino, Jalen Motes, Rebecca Rego

### Featured Music

1. Spitfire – The Prodigy (Featured in Opener)
2. Switch Up – Pretty Lights
3. Too Many Feelings – Javelin
4. Somesing – Sound Tribe Sector 9
5. Heart Skipped a Beat – The xx (Featured in Sexual Tension)
6. Contingent Being – SaReGaMa
7. Imitosis (Remix) – Andre Bird/Four Tet
8. Chewing Gum (Vocal Mix) – Annie
9. Let Me – Rhianna; (Featured in the O.C.)
10. Getting Freaky – Play N Skillz ft. Pitbull (Featured in the O.C.)
11. Arcadia – Apparat
12. Pattern – Zapan
13. Seattle – This is Art (Featured in Red Vs. Blue)
14. Deeper – Kid Static
15. A Vida - Cujo
16. Dee – Javelin
17. Sacred Place – Future of Forestry (Featured in Awkward Suicide)
18. Cosmic Cycles – This is Art
19. Selection from the Osmos soundtrack – composed by Vincent et Tristan, Julien Neto, Gas 0095, Loscil, High Skies (Featured in Zen      Tower)
20. Life and Times Intro – Josh Brechner
21. Upbeet – Bookmobile
22. A New Conviction – Ve
23. Letter from God to Man – Dan Le Sac vs. Scroobius Pip (Featured       in Closer)
24. Susie Cues - Javelin