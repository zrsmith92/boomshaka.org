---
layout: default
title: Hire Us
active_nav: Hire Us
---

Hire Boomshaka
==============

For information about availability and prices, please contact 
[producers@boomshaka.org](mailto:producers@boomshaka.org).

## School Gigs

Invite Boomshaka to play at your school! We perform for students of all ages and
 tailor our shows to the suit the interests and needs of each unique audience.

## Workshops

Boomshaka teaches workshops in drumming, dancing and body percussion. Book us 
for a show, then invite your audience to learn some of what they saw on stage!

## At the Concert Hall

Boomshaka performs on concert programs and for percussion and rhythm festivals 
throughout the Chicago area.

## Corporate Entertainment

Boomshaka is a great addition to an evening of entertainment at your corporate 
event.

## Special Events

Benefits, Sporting Events, Parades. You name it, we'll do it!