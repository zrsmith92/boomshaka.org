---
layout: about
title: History of Boomshaka
active_nav: About
active_subnav: History
---

History
=======

In 1997, Northwestern University drumline member Josh Berner wrote a STOMP-
inspired piece for Northwestern's annual, student-written musical Waa-Mu. They 
used pots pans and food trays from the campus dining halls and named the piece 
"Saga Stomp," taking the name from Northwestern students' common term for the 
campus food service provider, Saga. "Saga Stomp" combined complex rhythms, 
energetic choreography, and Northwestern humor in one wild piece and ran as one 
of the major highlights of the show. 

Fueled by the success of "Saga Stomp," Jade Smalls, fellow drumline member and 
performer in "Saga Stomp" joined Berner to start an independent performance 
troupe based on the same concepts later that year. With financial help from 
Northwestern's student-run production company WAVE, they formed Boomshaka and 
by 1999 staged its first show as headliners, Sometimes You've Just Gotta Put 
Your Foot Down. Despite being held on the same weekend as Northwestern's 
notorious music festival Dillo Day, both of the back-to-back performances sold 
out. 

The following winter, Boomshaka sold out all five performances of their second 
show Evolve, solidifying Boomshaka's presence on campus as a premier 
performance ensemble. During Spring Break 2000, Boomshaka established itself as 
a performance ensemble outside of the Northwestern community by touring the 
Northeast coast and performing in New York, Washington D.C. and Boston. Having 
grown into a yearlong performance ensemble, Boomshaka separated from Wave 
Productions in 2001 and produced its third show Be the Groove as an independent 
production company at Northwestern. Since then, Boomshaka has grown into an 
accomplished and established ensemble, performing across Northwestern, Chicago, 
and the country.

Notable Accomplishments
-----------------------

**May 2013**  Performed live on the Today Show

**Spring 2013** Performed at Houston Space Center

**Spring 2012** Performed at Bronx Museum of Arts

**Fall 2011** Performed at Chicago House of Blues

**June 2006** Awarded Northwestern University's "Outstanding Performing Arts 
Production" for 2006 Spring Show, Behind the Groove. 

**November 2005** Featured guest performers of Toshiba at the RSNA (Radiological
 Society of North America) 2005 Conference. 

**September 2004** Featured performers at the second annual CINEME International
 Animation Festival. 

**January 2004** Special guest performance with 7-time Grammy Award winning 
musician Paul Wertico and the Chicago Percussion All-Stars 

**December 2003** Recipient of the 2004 Center for Interdisciplinary Research in
 the Arts Grant 

**May 2003** Featured in performance with the Chicago Human Rhythm Project 

**March 2003** Interview and performance on MTV's Total Request Live 

**October 2002** First place winners in the dance division at the Chicago 
Cultural Center's "C2: College x Culture Competition" 
