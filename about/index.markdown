---
layout: about
title: About Boomshaka
active_nav: About
active_subnav: About Boomshaka
---

About Boomshaka
===============

Boomshaka is Northwestern University's premier drum, dance, and rhythm ensemble. 
Entirely student written, directed, and produced, Boomshaka has established a 
reputation for consistently delivering energetic and inspiring performances. 
We combine dance styles such as hip-hop and tap with various forms of percussion, 
using everything from sticks, poles, and buckets to our hands and feet. 

Our performers come from a variety of backgrounds and have a number of areas of 
artistic expertise, ranging from classical music composition and formal dance 
training to free style rapping and break dancing. We blend our unique talents and 
experiment with new mediums of expression to create a performance like none other! 

Please also visit [Be the Groove](http://www.bethegroove.com), a Chicago based group created by and featuring 
alumni members of Boomshaka.
