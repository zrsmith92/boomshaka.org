---
layout: about
title: Mission Statement
active_nav: About
active_subnav: Mission Statement
---

Mission Statement
=================

Boomshaka connects the worlds of dance, music, and percussion to form a unique 
performance ensemble driven by a love of rhythm. Whether performing at schools 
or staging impromptu shows on neighborhood streets, Boomshaka brings rhythm and 
energy to audiences across the country, inviting them to groove and find the 
beat that lies within us all.