---
layout: member
name: "Amy Lieberman"
photo: "amy_lieberman.jpg"
tags: cast_2014
---

Amy could not name one ballet term, but she's ready to dance and stomp her heart out as a Boomshaka Freshman! In high school, she spent four years on the dance team and ran distance track.  Born and raised in Wisconsin, she is a die-hard Packer fan, and at 4 feet 11 inches, she will still fight people who claim their team is better. She's a fierce snuggler and will eat just about anything you put in front of her.  #BOOM
