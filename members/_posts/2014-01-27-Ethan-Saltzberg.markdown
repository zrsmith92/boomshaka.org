---
layout: member
name: "Ethan Saltzberg"
photo: "ethan_saltzberg.jpg"
tags: cast_2013 cast_2014
---

Ethan is a sophomore Radio/Television/Film major in the School of Communications. He has been playing percussion since 3rd grade, and played drumset and marching snare from 8th grade until high school graduation.  He auditioned and was accepted to the front ensemble of Jersey Surf Drum & Bugle Corps, but unfortunately did not march that season. Ethan is from Oak Park, IL, but currently resides in Kennett Square, PA since 2007.  He is mainly interested in film/video production, but is also an avid music fan and oddly fascinated by languages.