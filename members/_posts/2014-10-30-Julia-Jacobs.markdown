---
layout: member
name: "Julia Jacobs"
photo: "julia_jacobs.jpg"
tags: cast_2014
---

Julia is currently a freshman in the Medill School of Journalism but refuses to let her passion for dance be snuffed out by the cold, dark Real World. She started training in ballet and jazz at the age of four and spent a decade at Virtuoso Performing Arts, a studio founded by three dancin' sisters. Julia has a love for all types of movement but leans towards the stuff with some rhythm and funk in her choreography.

When she's not dancing, Julia loves to read, write and geek-out listening to podcasts. Fact: Julia has read all seven Harry Potter books seven times each and now wishes that she had spent that time learning some sort of valuable skill. 