---
layout: member
name: "Andre Mohring"
tags: cast_2013 cast_2014 public_relations_2014
photo: "andre_mohring.jpg"
---

Andre’s spirit animal is an owl-lizard...take that as you will. Andre is a skier/musician/engineer and that about sums him right up. He was born and raised in Minneapolis, MN, and is currently studying design engineering at Northwestern. Music is his life (besides engineering oops calculus free body diagram CAD what). Andre has played guitar and drum set for a number of years and loves the two to death. He enjoys every corner of the music world from loud metal to jazz to some smooth indie jamz. Andre has also grown up surrounded by various folk and bluegrass music, so he’s a big fan of that as well. Additionally, Andre is very much a cat person.