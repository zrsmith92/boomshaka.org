---
layout: member
name: "Aviv Delgadillo"
tags: cast_2014
photo: "aviv_delgadillo.jpg"
---

Aviv is a freshman in McCormick School of Engineering and Applied Science from the San Francisco Bay Area.  Before coming to Northwestern, Aviv played drum set for 11 years, avidly performing with the rock band First Contact all throughout high school.  He also (occasionally) performs in musicals and other strange gatherings.

Though it seems as if all Aviv does is drum, he also has a particular fondness for good burritos, the Walking Dead, and his friends.  He listens to a number of different genres of music and secretly loves Ariana Grande.  He loves to meet new people and be exposed to new things, especially new types of food.  Someday, he hopes to learn how to dance.