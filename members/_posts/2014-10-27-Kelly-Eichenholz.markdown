---
layout: member
name: "Kelly Eichenholz"
photo: "kelly_eichenholz.jpg"
tags: cast_2014
---

Three facts about Kelly: 

1. Kelly has been groovin' since a young age; her original dancing partner Barney can attest to her mad skills.
2. Her soul is easy, breezy, beautiful -- it boils down to a simple equation of Texan charm and a winning smile. Plus, she gives great hugs. 
3. Meow meow go 'Cats (she loves her school) 