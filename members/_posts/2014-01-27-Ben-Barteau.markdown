---
layout: member
name: "Ben Barteau"
tags: cast_2013 cast_2014 equipment_managers_2014
photo: "ben_barteau.jpg"
---

Ben started drumming eight years ago, and has been playing in theaters, parks, bars, weddings, churches, and the occasional basement ever since. He plays local gigs with his band in his hometown of Rockford, IL, but while at Northwestern, Boomshaka is his drumming outlet. Currently studying Materials Science in the McCormick School of Engineering, he hopes to focus in Nanotechnology and one day... do... something with that... but he's pretty sure it will work out.