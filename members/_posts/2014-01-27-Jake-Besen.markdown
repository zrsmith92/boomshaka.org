---
layout: member
name: "Jake Besen"
photo: "jake_besen.jpg"
tags: cast_2011 cast_2012 cast_2013 cast_2014
---

Jake is a senior studying to be a mechanical engineer. He has been playing the drums, guitar, bass, and ukelele since 8th grade. The only thing he loves more than food or sleep is making music. Usually when he’s zoning out during class, he is thinking about song ideas or a cool drum part or something, and then the second Jake’s out of class he’s trying to recreate that idea on guitar and drums. Jake also loves jamming and improving with his friends because “no one has to talk and we’re all tuned in on a super deep level”.

TL;DR "Jake's a uke-playing womanizer who also likes longboarding and Chicken Shack"