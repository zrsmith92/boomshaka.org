---
layout: member
name: "Grant Pender"
photo: "grant_pender.jpg"
tags: cast_2014
---

Grant Pender, hailing from Naperville, IL, is psyched to be a freshman at Northwestern University.  He started studying on the drum set at age 8.  In fifth grade he also began studying bass.  His broad musical experiences range anywhere from playing in rock/blues bands, to jazz ensembles, to symphony orchestras.  When he's not performing on stage, he is constantly listening to music (seriously, he had 31,863 minutes of Spotify usage in 2014) and enjoys going to concerts.  Other than music he likes binge watching shows and random documentaries on Netflix, being outdoors, traveling, and eating Chipotle.
