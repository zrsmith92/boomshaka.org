---
layout: member
name: "Allison Stenclik"
photo: "allie_stenclik.jpg"
tags: cast_2012 cast_2013 public_relations_2013 cast_2014 public_relations_2014
---

Born and raised in the city of notoriously good wings and infamously awful football, Allie upgraded from Buffalo, NY, to Northwestern to study Communications and Comparative Literature. The current junior has been dancing since age seven and has an embarrassing large collection of hideous dance costumes to prove it. Most of her training is in ballet, classic modern techniques (primarily Graham, Cunningham, and Limon), and musical theatre dance, but has tried most other styles at one point or another. Nowadays, she spends her time flailing on a yoga mat, pining over recipes she'll never make, and pursuing social media endeavors that showcase (re: embarrass) other cast members for Facebook likes.