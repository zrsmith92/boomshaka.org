---
layout: member
name: "Zach Hyman"
photo: "zach_hyman.jpg"
tags: cast_2011 cast_2012 equipment_managers_2012 cast_2013 directors_2013 cast_2014 directors_2014
---

Zach Hyman is a rhythm and drumming enthusiast moonlighting as a senior film major on the weekends. He was assigned to the snare drum in his school band as a fifth grader and never looked back, continuing to practice and perform and generally be "that guy" who obnoxiously taps out beats on his desk during class. Zach feels overall uneasy with his transition into adulthood and wonders from time to time why people trust him with any sort of responsibility at all.