---
layout: member
name: "Charlie Scott"
photo: "charlie_scott.jpg"
tags: cast_2012 cast_2013 cast_2014
---

Charlie is a senior majoring in RTVF in the School of Communications, minoring in Music Technology. He's been drumming since the age of 14, and also plays saxophones, sousaphone/tuba, bass guitar, and sings. He has absolutely zero dancing instruction.

He has a black belt in Tae Kwon Do and swagger. His full name is Roland Charles Hamilton Scott and his eyes are green _and_ brown.