---
layout: member
name: "Jalen Motes"
photo: "jalen_motes.jpg"
tags: cast_2010 cast_2011 cast_2012 cast_2013 equipment_managers_2012
---

We can only let Jalen speak for himself...

> Yo. I'm Jalen Motes, Communication Studies Major in the School of Communication (redundant, I know), graduating in 2014. I've loved rhythm for as long as I've loved anything else---some of my earliest memories include learning to clap in time with the music my parents played in the car. But despite having been formally trained to play many percussion instruments over the last ten years, I'd say that I am---at best---a drum enthusiast with a charming on-stage personality. My main goal in life is to promote thoughtfulness, understanding, and optimism among those with whom I come in contact. Lofty as it may seem, I really want to change the world! And I believe any amount of change in a positive direction is probably good for humankind---no matter how small it may be. Right now though, my main projects include my rapper/composer/musician ethos named Caeto Moon and my music criticism/thought-space blog, [www.anyvibes.com](http://anyvibes.com). I'm still working to sand down my rough edges, but: such is the stuff of life, non? And who knows where all this'll take me? Hopefully to the bank! Hah! No, but really: I hope to someday be a notable advocate for environmentalism and practical education for the youth in the arts and sciences.