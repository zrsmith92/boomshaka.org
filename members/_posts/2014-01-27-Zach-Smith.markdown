---
layout: member
name: "Zach Smith"
photo: "zach_smith.jpg"
tags: cast_2010 cast_2011 cast_2012 directors_2012 cast_2013 directors_2013 cast_2014
---

Zach Smith is a Computer Engineering major at NU. Music has been a significant part of his life since beginning piano lessons at age 5. At age 8, his older brother began teaching him
how to play the drumset. He quickly surpassed his brother in both good looks and drumming prowess (that got awkward). Zach went on to participate in his high school drumline, as well as the 
Northwestern University drumline for four years.

Besides drumming, Zach enjoys a good bowl of mac 'n' cheese, developing new, ground-breaking shoulder-only dance moves, and perfecting his (soon-to-be-debuted) Creed tribute band, Creedefined.