---
layout: member
name: "Simon Barnicle"
photo: "simon_barnicle.jpg"
tags: cast_2012 cast_2013 equipment_managers_2013 cast_2014 directors_2014
---

Simon is in SESP/Weinberg class of 2016. He's been drumming since high school, and also describes himself as a below-average trombone and guitar player. His hobbies include fantasy football and Taco Bell.