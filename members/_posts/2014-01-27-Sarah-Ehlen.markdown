---
layout: member
name: "Sarah Ehlen"
photo: "sarah_ehlen.jpg"
tags: cast_2012 cast_2013 cast_2014 directors_2014
---

Sarah is a junior studying journalism. Sarah started dancing in elementary school with the classic tap, jazz and ballet combo, but over the years, she also trained in other genres of dance including lyrical, contemporary, hip hop, pointe, and modern. Sarah danced at a competition dance studio until she graduated from high school and also competed on her high school's dance team.

After being born and raised in Minnesota, Sarah can say that she has successfully memorized the layout of the Mall of America and has gone swimming in approximately 10,000 lakes (give or take a few). She sometimes wishes Hannah Montana would make a comeback so she could star as her dance-double in her hit television series, and Sarah wouldn't be unhappy if you addressed her as Her Majesty from time to time. Sarah also holds the record for world's most proficient selfie photographer and spends most of her free time trying to get Simon Barnicle to admit they're besties.