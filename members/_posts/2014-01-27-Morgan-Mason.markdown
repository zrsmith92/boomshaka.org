---
layout: member
name: "Morgan Mason"
photo: "morgan_mason.jpg"
tags: cast_2013 cast_2014
---

Morgan is currently a sophomore in the Weinberg College of Arts and Sciences planning to double major in Economics and Mathematics. She trained for four years in classical ballet before training under Pierre Lockett in the Joffrey Ballet Exelon Strobel Step-Up Program. 

Morgan absolutely loves Batman, and she currently has about 200 comics, most of which are about him or someone in the Batman Family.  Her favorite color is green and she also really likes French Fries…and chocolate. 