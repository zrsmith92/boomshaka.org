---
layout: member
name: "Justin Lueker"
tags: cast_2009 cast_2010 cast_2011 cast_2012 cast_2013 equipment_managers_2011
---

Justin is a senior-year Civil Engineering and Music Composition double major. His training includes “all kinds of fun things”. Justin started playing piano as a youngster, which makes him very unique at Northwestern (jk). A handful of woodwinds here and there - Justin is praised throughout Beaverton, Oregon as the multi-instrumentalist in Sunset High School's 2009 production of Urinetown. Justin played in drumline for years and years, and has played Go U Northwestern on tenor drums approximately 10 billion times. After joining Boomshaka, Justin is pleased to admit that his favorite hobby is generating sultry percussive tones from a 44 gallon trash recepticle. As the saying goes, dig stuff out of dumpsters and the happiness will follow. #AskMeAboutMyMattress As for dance, he is  equally learned in all styles...

Justin enjoys the outdoors and wild animals, and he thinks Evanston would benefit from a resident pack of wolves to control the rabbit population. Justin can out-pogo stick anyone at Northwestern, and he really loves creamy-chicken flavored ramen. And for those that are wondering, Justin secretly let Paul win on purpose in the legendary drumming standoff of Boomshaka Spring Show 2013, just cause he is a nice guy. DJUSTIN SHALL ALWAYS REMAIN UNCHAINED.