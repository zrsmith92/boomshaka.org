---
layout: member
name: "Juandiego Marquez"
photo: "juandiego.jpg"
tags: cast_2012 cast_2013 cast_2014
---

Juandiego Marquez is a senior studying Math and Psychology in Weinberg. He picked up drumming in high school by playing in his high school’s jazz band. He also plays a variety of brass instruments but his main instrument is the piano. When he is not drumming for Boomshaka, Juan’s hobbies include composing on the piano, being the best Pokemon master that no one ever was, and building up the world’s largest collection of medium crew-neck t-shirts.