---
layout: member
name: "Adina Nadler"
photo: "adina_nadler.jpg"
tags: cast_2013 cast_2014
---

Adina is a sophomore in Weinberg College of Arts and Sciences, from Ann Arbor, Michigan. She's been dancing ever since she was little, mostly because her older sister did it and she just really wanted to be like her. She is mainly trained in jazz, but there have been a couple years of hip-hop/ballet/contemporary thrown in there. In high school Adina was a part of a group called "Dance Body", which is an essentially student-led company where everyone is responsible for choreographing pieces and putting a show together.  She was also very involved in musical theater, which she really enjoys as well.

Adina loves performing and being on stage and no matter what she ends up doing in life, she wants that to be a part of it in at least some small way. She also really like cats.