---
layout: member
name: "Elisa Meyer"
tags: cast_2013 cast_2014
photo: "elisa_meyer.jpg"
---

Elisa Meyer was born in Chicago, Illinois on April 20, 1995. Her first debut on the stage occurred at the age of 7, when she starred as Mama Cheetah at zoo camp in Lincoln Park. It was then that everyone knew she was destined for stardom. At the age of 8 she began her dance training at Ruth Page Center for the Arts, studying ballet, modern, jazz, and pointe. She continued this training up until her freshman year of high school, when she joined Whitney Young Magnet High School's Guys and Dolls Contemporary Dance Team. Sophomore year of high school Elisa was accepted to the Joffrey Academy of dance, where she trained for two years. Throughout high school, during her summers Elisa had the opportunity to train with highly esteemed teachers, choreographers and dancers at Point Park University, l'Academie Americaine de Danse de Paris, Dance Italia, Thodos Dance and Mark Morris. Elisa is ecstatic to be at Northwestern University today. She is part of Weinberg's class of 2017, on the pre-med track and studying dance. She would like to make a special shout out to the people who made her who she is today, Mom, Dad, Jack, Frankie, Beyonce, and her roommate.